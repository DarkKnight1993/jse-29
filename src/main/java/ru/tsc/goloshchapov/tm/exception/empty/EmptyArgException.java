package ru.tsc.goloshchapov.tm.exception.empty;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class EmptyArgException extends AbstractException {

    public EmptyArgException() {
        super("Exception! Arg is empty!");
    }

}
