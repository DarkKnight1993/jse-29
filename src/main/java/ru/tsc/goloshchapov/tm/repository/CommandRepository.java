package ru.tsc.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.repository.ICommandRepository;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        return commands.values()
                .stream()
                .filter(c -> c.name() != null)
                .map(e -> e.name())
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<String> getCommandArg() {
        return commands.values()
                .stream()
                .filter(a -> a.arg() != null)
                .map(e -> e.arg())
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable String name) {
        return commands.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@Nullable String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
