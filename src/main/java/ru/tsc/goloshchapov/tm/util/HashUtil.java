package ru.tsc.goloshchapov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.setting.ISaltSettings;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @Nullable
    static String salt(@Nullable final ISaltSettings settings, @Nullable final String value) {
        if (settings == null) return null;
        @Nullable final String secret = settings.getPasswordSecret();
        @Nullable final Integer iteration = settings.getPasswordIteration();
        return salt(value, iteration, secret);
    }

    @Nullable
    static String salt(@Nullable final String value, @NotNull Integer iteration, @NotNull String secret) {
        if (value == null) return null;
        String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            @NotNull MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
