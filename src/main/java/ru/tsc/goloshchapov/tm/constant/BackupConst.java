package ru.tsc.goloshchapov.tm.constant;

public class BackupConst {

    public static final String BACKUP_SAVE = "data-backup-save";

    public static final String BACKUP_LOAD = "data-backup-load";

}
