package ru.tsc.goloshchapov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.goloshchapov.tm.constant.BackupConst;

public class Backup extends Thread {

    @NotNull
    final Bootstrap bootstrap;

    @NotNull
    private static final int INTERVAL = 30000;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.runCommand(BackupConst.BACKUP_LOAD);

    }

    public void save() {
        bootstrap.runCommand(BackupConst.BACKUP_SAVE);
    }

}
