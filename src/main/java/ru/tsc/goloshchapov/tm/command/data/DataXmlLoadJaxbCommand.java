package ru.tsc.goloshchapov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.command.AbstractDataCommand;
import ru.tsc.goloshchapov.tm.dto.Domain;
import ru.tsc.goloshchapov.tm.exception.system.FileIsNotExistException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataXmlLoadJaxbCommand extends AbstractDataCommand {
    @Override
    public @NotNull String name() {
        return "data-load-xml-jaxb";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String description() {
        return "Load data in xml format by jaxb";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML LOAD BY JAXB]");
        @NotNull final File file = new File(FILE_JAXB_XML);
        if (!file.exists()) throw new FileIsNotExistException(FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
        System.out.println("[OK]");
    }
}
