package ru.tsc.goloshchapov.tm.api;

import ru.tsc.goloshchapov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
