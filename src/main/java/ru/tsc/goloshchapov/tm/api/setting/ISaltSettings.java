package ru.tsc.goloshchapov.tm.api.setting;

public interface ISaltSettings {

    Integer getPasswordIteration();

    String getPasswordSecret();

}
